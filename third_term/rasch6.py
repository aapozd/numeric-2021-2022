import math
import numpy as np
from decimal import *
import sympy as sym

getcontext().prec = 7

x1, x2 = sym.symbols('x1 x2')

fun1sym = sym.tan(-1.2*x1 + x2) + 1.2*x1*x2 - 0.3
fun2sym = sym.sqrt(2.1 - x2**2)
fun3sym = x1**2 + x2**2 - 2.1

step = Decimal("0.2")
start = Decimal("0")
stop = Decimal("1.4") + step

xvals = np.arange(start, stop, step)
yvals = [fun2sym.evalf(subs={x2:i}) for i in xvals]

for x, y in zip(xvals, yvals):
    print(x, y, fun1sym.evalf(subs={x1:x, x2:y}))

print("\n\n")
H = [
    sym.diff(fun1sym, x1), 
    sym.diff(fun1sym, x2), 
    sym.diff(fun3sym, x1), 
    sym.diff(fun3sym, x2)
]

eps1, eps2 = sym.symbols('eps1, eps2')
x_matr_prev = {x1:1.2, x2:0.81}
eps_solution = {eps1: 0.028771258, eps2: -0.040216678}
f_eval = [0.144422108, -0.0039]

for i in range(4):
    x_matr_new = {x1:x_matr_prev[x1] + eps_solution[eps1], x2:x_matr_prev[x2] + eps_solution[eps2]}
    print(x_matr_new)
    f_eval = [fun1sym.evalf(subs=x_matr_new), fun3sym.evalf(subs=x_matr_new)]
    print(f_eval)
    h_solution = [func.evalf(subs=x_matr_new) for func in H]
    print(h_solution)
    eq1 = h_solution[0]*eps1 + h_solution[1]*eps2 + f_eval[0]
    eq2 = h_solution[2]*eps1 + h_solution[3]*eps2 + f_eval[1]
    eps_solution = sym.solve((eq1, eq2), eps1, eps2)
    print(eps_solution)
    x_matr_prev = x_matr_new
    print("\n\n")

print("TRUE SOLUTION")
print(sym.nsolve((fun1sym, fun3sym), [x1, x2], [1, 1]))

eq = 2.4*((0.144422108+2.971608288*eps2)/0.865929946)+1.62*eps2 - 0.0039
a = sym.solve(eq, eps2)
print(a)
eq = eps1 - ((0.144422108+2.971608288*a[0]))/0.865929946
print(sym.solve(eq, eps1))






