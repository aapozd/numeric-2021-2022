from listings import *
import matplotlib
from utils import *

if __name__ == "__main__":
  add_table_format()
  plt.style.use('_mpl-gallery')
  
  lab = Lab()

  tables_x_k = {
    "fun_x_k_vals": lab.fun_x_k_vals
  }

  tables_y_k = {
    "fun_y_k_vals": lab.fun_y_k_vals,
    "fun_lagrange_y_k_vals": lab.fun_lagrange_y_k_vals,
    "fun_spline_y_k_vals": lab.fun_spline_y_k_vals
  }

  args_array = ndarray_to_str(lab.x_k_vals)
  print_table("%s_table" % list(tables_x_k.items())[0][0], args_array, list(tables_x_k.items())[0][1])

  args_array = ndarray_to_str(lab.y_k_vals)
  for item in tables_y_k.items():
    print_table("%s_table" % list(item)[0], args_array, list(item)[1])

  funcs = [
    fun,
    lab.fun_lagrange_polynomial,
    lab.fun_spline
  ]
  for fun in funcs:
    print_plot(fun)

  print_quanc_results(lab.limit_1, lab.limit_2, lab.param_1, lab.quanc_integ_fun_1(), "param_1")
  print_quanc_results(lab.limit_1, lab.limit_2, lab.param_2, lab.quanc_integ_fun_2(), "param_2")
  