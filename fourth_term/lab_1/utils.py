import tabulate as tbl
from tabulate import tabulate
from functools import partial
import re
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def border_line_begin_tabular(colwidths, colaligns, booktabs=False, longtable=False):
  alignment = {"left": "|l", "right": "|r", "center": "|c", "decimal": "|r"}
  tabular_columns_fmt = "".join([alignment.get(a, "l") for a in colaligns]) + "|"
  return "\n".join(
    [
      ("\\begin{tabular}{" if not longtable else "\\begin{longtable}{")
      + tabular_columns_fmt
      + "}",
      "\\toprule" if booktabs else "\\hline",
    ]
  )

def print_table(filename, args_array, vals_array):
  rows = []
  for arg, val in zip(args_array, vals_array):
    rows.append([arg, val])
  with open("tables/%s.tex" % filename, 'w') as f:
      table = tabulate(rows, ["Зн. арг.", "Функция"], tablefmt="latex_border_longtable")
      print(table, file = f)

def print_plot(fun):
  ax_vals = np.linspace(0, 3, 100)
  # Создание файла.
  pdf = PdfPages("fig/%s.pdf" % fun.__name__)
  fig, ax = plt.subplots()
  ax.plot(ax_vals, [fun(i) for i in ax_vals])
  ax.set(xlim=(0, 3), xticks=np.arange(0, 3, 0.3), ylim=(0, 3), yticks=np.arange(0, 3, 0.3))
  pdf.savefig()
  plt.close()
  # Сохранение файла
  pdf.close()

def ndarray_to_str(array):
  # require dtype to be floating point
  return re.findall(r'[0-9]+\.[0-9]*', np.array_str(array))

def print_quanc_results(limit_1, limit_2, param, quanc_results, filename):
  formula = "\[ \int_{limit_1}^{limit_2} |sin(x) - 0.6|^{param} \,dx  = {value} \]"
  with open("report/%s_integral.tex" % filename, 'w') as f:
    print(formula.format(
      limit_1 = "{" + str(limit_1) + "}",
      limit_2 = "{" + str(limit_2) + "}",
      param = "{" + str(param) + "}", 
      value = quanc_results["result"]), file = f
    )

  args = ["abserr", "relerr", "nofun", "posn", "flag", "errest"]
  print_table("%s_table" % filename, args, [quanc_results[res] for res in args])

def add_table_format():
  form = tbl._table_formats["latex_longtable"]
  tbl._table_formats["latex_border_longtable"] = tbl.TableFormat(
    lineabove=partial(border_line_begin_tabular, longtable=True),
    linebelowheader=tbl.Line("\\hline\n\\endhead", "", "", ""),
    linebetweenrows=None,
    linebelow=tbl.Line("\\hline\n\\end{longtable}", "", "", ""),
    headerrow=tbl._latex_row,
    datarow=tbl._latex_row,
    padding=1,
    with_header_hide=None,
  )