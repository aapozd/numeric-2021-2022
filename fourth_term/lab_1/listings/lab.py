import math
import libfmm_boost as fmm
import numpy as np

pw_int = fmm.primitive_wrapper_int
pw_double = fmm.primitive_wrapper_double

def lagrange_polynomial(n_points, x_vals, y_vals, xn):
  sum = 0
  for i in range(0, n_points, 1):
    mult = 1
    for j in range(0, n_points, 1):
      if i != j:
        mult = mult * (xn - x_vals[j])/(x_vals[i] - x_vals[j]); 
    sum = sum + mult * y_vals[i]
  return sum

def fun(x):
  return 1 - math.exp(-x)

def integ_fun(x, m):
  return np.double((abs(np.sin(x) - 0.6))**m)

def quanc_integ_fun(limit_1, limit_2, param):
  resultR = pw_double(0)
  errestR = pw_double(0)
  nofunR = pw_int(0)
  posnR = pw_double(0)
  flag = pw_int(0)
  abserr = np.double(1e-6)
  relerr = np.double(1e-6)
  fun = lambda x: integ_fun(x, param)
  fmm.quanc8(fun, limit_1, limit_2, abserr, relerr, resultR, errestR, nofunR, posnR, flag)
  return {
    "result": resultR.get_val(), 
    "abserr": abserr, 
    "relerr": relerr, 
    "nofun": nofunR.get_val(), 
    "errest": errestR.get_val(),
    "posn": posnR.get_val(), 
    "flag" : flag.get_val()
  }

class Lab:
  x_k_n_points = 11
  x_k_step = 0.3
  x_k_start = 0.0
  x_k_vals = np.arange(x_k_start, x_k_start + x_k_step * x_k_n_points, x_k_step)
  fun_x_k_vals = np.array([fun(i) for i in x_k_vals])

  y_k_n_points = 10
  y_k_step = 0.3
  y_k_start = 0.15
  y_k_vals = np.arange(y_k_start, y_k_start + y_k_step * y_k_n_points, y_k_step)

  param_1 = -1
  param_2 = -0.5

  limit_1 = np.double(0.5)
  limit_2 = np.double(1)

  def __init__(self):
    #values used in table
    self.fun_y_k_vals = [fun(i) for i in Lab.y_k_vals]
    self.fun_lagrange_y_k_vals = [self.fun_lagrange_polynomial(i) for i in Lab.y_k_vals]
    self.spline_coef = self.splrep(Lab.x_k_n_points, Lab.x_k_vals, Lab.fun_x_k_vals)
    self.fun_spline_y_k_vals = [self.fun_spline(i) for i in Lab.y_k_vals]

  def splrep(self, n_points, x_vals, y_vals):
    b = np.array([0 for i in range(n_points)]).astype(np.double)
    c = np.array([0 for i in range(n_points)]).astype(np.double)
    d = np.array([0 for i in range(n_points)]).astype(np.double)
    flag = pw_int(0)
    fmm.spline(n_points, 0, 0, np.double(0), np.double(0), x_vals.astype(np.double), 
        y_vals.astype(np.double), b, c, d, flag)
    
    return {"b": b, "c": c, "d": d}

  def seval(self, n_points, xn, x_vals, y_vals, b, c, d, last):
    return fmm.seval(n_points, xn, x_vals.astype(np.double), y_vals.astype(np.double), 
        b, c, d, last)
  
  def fun_lagrange_polynomial(self, xn):
    return lagrange_polynomial(Lab.x_k_n_points, Lab.x_k_vals, Lab.fun_x_k_vals, xn)

  def fun_spline(self, xn):
    last = pw_int(0)
    return self.seval(Lab.y_k_n_points, xn, Lab.x_k_vals, Lab.fun_x_k_vals, self.spline_coef["b"], self.spline_coef["c"],
        self.spline_coef["d"], last)

  def quanc_integ_fun_1(self):
    return quanc_integ_fun(Lab.limit_1, Lab.limit_2, Lab.param_1)
    
  def quanc_integ_fun_2(self):
    return quanc_integ_fun(Lab.limit_1, Lab.limit_2, Lab.param_2)

if __name__ == "__main__":
  lab = Lab()
  # tables
  print(lab.fun_x_k_vals)
  print(lab.fun_y_k_vals)
  print(lab.fun_lagrange_y_k_vals)
  print(lab.fun_spline_y_k_vals)
  # integrals
  print(lab.quanc_integ_fun_1())
  print(lab.quanc_integ_fun_2())