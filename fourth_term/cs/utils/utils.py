import csv

def write_csv(file_name, solutions, precisions):
  field_names = ["x", "z1", "z2"]
  with open(file_name, 'w', newline = '') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames = field_names)
    writer.writeheader()

    for row in solutions:
      writer.writerow({
        "x": format(row[0], '.{p}f'.format(p = precisions[0])), 
        "z1": format(row[1], '.{p}f'.format(p = precisions[1])), 
        "z2": format(row[2], '.{p}f'.format(p = precisions[2]))
        })