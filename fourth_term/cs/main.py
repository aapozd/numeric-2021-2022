import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from listings import *
from utils import *

def fprint(string, file = None):
  if file != None:
    with open(file, "w") as f:
      print(string, file = f)
  else:
    print(string)

if __name__ == "__main__":
  lab = Lab()
  fprint(format(lab.shoot_b, '.{p}f'.format(p = lab.precision)), "values/result.tex")
  fprint("{:e}".format(lab.b_abserr), "values/abserr.tex")
  fprint(format(lab.root, '.{p}f'.format(p = lab.precision)), "values/root.tex")
  fprint(format(lab.root_err_plus, '.{p}f'.format(p = lab.precision)), "values/root_err_plus.tex")
  fprint(format(lab.root_err_neg, '.{p}f'.format(p = lab.precision)), "values/root_err_neg.tex")

  fprint("{:e}".format(lab.z_1_plus_mid_square), "values/z_1_plus_mid_square.tex")
  fprint("{:e}".format(lab.z_1_neg_mid_square), "values/z_1_neg_mid_square.tex")
  fprint("{:e}".format(lab.z_2_plus_mid_square), "values/z_2_plus_mid_square.tex")
  fprint("{:e}".format(lab.z_2_neg_mid_square), "values/z_2_neg_mid_square.tex")

  write_csv("csv/solutions.csv", lab.solutions, (lab.x_precision, lab.precision, lab.precision))
  write_csv("csv/solutions_err_plus.csv", lab.solutions_err_plus, (lab.x_precision, lab.precision, lab.precision))
  write_csv("csv/solutions_err_neg.csv", lab.solutions_err_neg, (lab.x_precision, lab.precision, lab.precision))
  
  pdf = PdfPages("fig/solutions.pdf")
  fig = plt.figure()
  ax = plt.axes()

  xline = [row[1] for row in lab.solutions]
  yline = [row[2] for row in lab.solutions]
  ax.plot(xline, yline, 'red', label = "$z$")

  xline = [row[1] for row in lab.solutions_err_plus]
  yline = [row[2] for row in lab.solutions_err_plus]
  ax.plot(xline, yline, 'green', label = "$z*$")

  xline = [row[1] for row in lab.solutions_err_neg]
  yline = [row[2] for row in lab.solutions_err_neg]
  ax.plot(xline, yline, 'blue', label = "$z**$")
  
  ax.legend()
  plt.xlabel("$z^{(1)}$")
  plt.ylabel("$z^{(2)}$")
  plt.grid()
  pdf.savefig()
  plt.close()
  pdf.close()
