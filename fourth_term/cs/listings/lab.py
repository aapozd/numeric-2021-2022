import numpy as np
from scipy.integrate import quad, RK45
from scipy.optimize import root_scalar
from prettytable import PrettyTable

def b_fun(x):
  return np.exp(-x) / np.power(x, 3)

def diff_eq_fun(x, z):
  return np.array([
    0 * z[0] + np.power(z[1], 2) - 1,
    z[0] + 0 * z[1]
  ])

'''
h_print - шаг печати
t_0, t_1 - границы промежутка
y_0 - вектор начальных условий
'''
def solve_rkf_2(h_print, t_0, t_1, y_0, eps):
  solutions = [(t_0, y_0[0], y_0[1])]
  t_bound = h_print
  for j in np.arange(h_print, t_1 + h_print, h_print):
    rkf45 = RK45(diff_eq_fun, t0 = t_0, y0 = y_0, t_bound = t_bound, atol = eps, rtol = eps)
    t_bound += h_print
    while rkf45.status != "finished":
      rkf45.step()
    solutions.append((rkf45.t, rkf45.y[0], rkf45.y[1]))

  return np.array(solutions)

'''
t_0, t_1 - границы промежутка
y_0_1 = z^(2)(0) - начальное условие
y_1_1 = z^(2)(1)
shoot_a, shoot_b - диапазон стрельбы
Пристрелка по y_0_0 = z^(1)(0)
'''
def solve_boundary(t_0, t_1, y_0_1, y_1_1, shoot_a, shoot_b, eps):
  def l_b_ua(y_0_0):
    t_bound = t_1 - t_0
    rkf45 = RK45(diff_eq_fun, t0 = t_0, y0 = [y_0_0, y_0_1], t_bound = t_bound, atol = eps, rtol = eps)
    while rkf45.status != "finished":
      rkf45.step()
    return rkf45.y[1] - y_1_1 # z^(2)(1)* - z^(2)(1)
    
  return root_scalar(l_b_ua, method = "bisect", bracket = (shoot_a, shoot_b)).root

def get_col(A, col):
  return [row[col] for row in A]

def get_mid_square(A, B):
  return sum([(a - b)**2 for a, b in zip(A, B)]) / len(A)

class Lab:
  A = 1.2
  B_int_limit_0 = 0.1
  B_int_limit_1 = 0.2
  t_0 = 0
  t_1 = 1
  y_0_1 = 0
  y_1_1 = 1
  shoot_a = 1.2
  eps = 1e-6
  delta = 0.1
  h_print = 0.1
  precision = 12
  x_precision = 2
  
  def __init__(self):
    self.shoot_b, self.b_abserr = quad(b_fun, Lab.B_int_limit_0, Lab.B_int_limit_1, epsabs = Lab.eps)
    self.shoot_b = 0.046 * self.shoot_b
    
    self.err_y_0_1 = Lab.y_0_1 + Lab.delta
    self.err_y_1_1 = Lab.y_1_1 + Lab.delta

    self.root = solve_boundary(Lab.t_0, Lab.t_1, Lab.y_0_1, Lab.y_1_1, Lab.shoot_a, self.shoot_b, Lab.eps)
    self.root_err_plus = 1.01 * self.root
    self.root_err_neg = 0.99 * self.root

    self.solutions = solve_rkf_2(Lab.h_print, Lab.t_0, Lab.t_1, [self.root, Lab.y_0_1], Lab.eps)
    self.solutions_err_plus = solve_rkf_2(Lab.h_print, Lab.t_0, Lab.t_1, [self.root_err_plus, Lab.y_0_1], Lab.eps)
    self.solutions_err_neg = solve_rkf_2(Lab.h_print, Lab.t_0, Lab.t_1, [self.root_err_neg, Lab.y_0_1], Lab.eps)

    z_1_raw = get_col(self.solutions, 1)
    z_1_plus = get_col(self.solutions_err_plus, 1)
    z_1_neg = get_col(self.solutions_err_neg, 1)

    z_2_raw = get_col(self.solutions, 2)
    z_2_plus = get_col(self.solutions_err_plus, 2)
    z_2_neg = get_col(self.solutions_err_neg, 2)

    self.z_1_plus_mid_square = get_mid_square(z_1_raw, z_1_plus)
    self.z_1_neg_mid_square = get_mid_square(z_1_raw, z_1_neg)
    self.z_2_plus_mid_square = get_mid_square(z_2_raw, z_2_plus)
    self.z_2_neg_mid_square = get_mid_square(z_2_raw, z_2_neg)

def print_table(solutions):
  x = PrettyTable()
  x.field_names = ["x", "z1", "z2"]
  x.add_rows(solutions)
  print(x)

if __name__ == "__main__":
  lab = Lab()
  print("result = {result}".format(result = lab.shoot_b))
  print("abserr = {abserr}".format(abserr = lab.b_abserr))

  print("root = {root}".format(root = lab.root))

  print("z_1_plus_mid_square = {mid_square}".format(mid_square = lab.z_1_plus_mid_square))
  print("z_1_neg_mid_square = {mid_square}".format(mid_square = lab.z_1_neg_mid_square))
  print("z_2_plus_mid_square = {mid_square}".format(mid_square = lab.z_2_plus_mid_square))
  print("z_2_neg_mid_square = {mid_square}".format(mid_square = lab.z_2_neg_mid_square))

  print_table(lab.solutions)
  print_table(lab.solutions_err_plus)
  print_table(lab.solutions_err_neg)
