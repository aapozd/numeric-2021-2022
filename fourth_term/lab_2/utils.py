import tabulate as tbl
from tabulate import tabulate
from functools import partial
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def fprint(string, file = None):
  if file != None:
    with open(file, "w") as f:
      print(string, file = f)
  else:
    print(string)

def print_matrix(matrix, file = None):
  string = "\\[ \\begin{pmatrix}\n"
  num_rows, num_cols = matrix.shape
  for i in range(num_rows):
    for j in range(num_cols):
      string += str(matrix[i][j]) + " "
      if not((num_cols - j) == 1):
        string += "& "
    string += "\\\\\n"
  string += "\\end{pmatrix} \\]\n"
  
  fprint(string, file)

def print_r_norm(norm, p, file = None):
  string = "\\[ \( ||A(%f)^{-1}|| = %f \) \\]" % (p, norm)
  fprint(string, file)

def print_r_cond(cond, p, file = None):
  string = "\\[ \( cond(A(%f)^{-1}) = %f \\]" % (p, cond)
  fprint(string, file)

def border_line_begin_tabular(colwidths, colaligns, booktabs=False, longtable=False):
  alignment = {"left": "|l", "right": "|r", "center": "|c", "decimal": "|r"}
  tabular_columns_fmt = "".join([alignment.get(a, "l") for a in colaligns]) + "|"
  return "\n".join(
    [
      ("\\begin{tabular}{" if not longtable else "\\begin{longtable}{")
      + tabular_columns_fmt
      + "}",
      "\\toprule" if booktabs else "\\hline",
    ]
  )
  
def add_table_format():
  form = tbl._table_formats["latex_longtable"]
  tbl._table_formats["latex_border_longtable"] = tbl.TableFormat(
    lineabove=partial(border_line_begin_tabular, longtable=True),
    linebelowheader=tbl.Line("\\hline\n\\endhead", "", "", ""),
    linebetweenrows=None,
    linebelow=tbl.Line("\\hline\n\\end{longtable}", "", "", ""),
    headerrow=tbl._latex_row,
    datarow=tbl._latex_row,
    padding=1,
    with_header_hide=None,
  )

def print_table(filename, p_arr, a_cond_num_arr, r_norm_arr):
  rows = []
  for p, a_cond_num, r_norm in zip(p_arr, a_cond_num_arr, r_norm_arr):
    rows.append([p, a_cond_num, r_norm])
  with open(filename, 'w') as f:
      table = tabulate(rows, ["Зн. p", "cond(A)", "||R||"], tablefmt="latex_border_longtable")
      print(table, file = f)