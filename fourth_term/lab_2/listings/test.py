import unittest
import lab
import numpy as np
from scipy.linalg import lu_factor, lu_solve

class TestLab(unittest.TestCase):
  def test_gen_e_raises_neg(self):
    with self.assertRaises(ValueError):
      lab.gen_e(-5)

  def test_gen_e_matrix_zero(self):
    self.assertTrue(np.array_equal(lab.gen_e(0), np.array([])))

  def test_gen_e_pos_shape(self):
    magicNumber = 10
    e = lab.gen_e(magicNumber)
    num_rows, num_cols = e.shape
    self.assertTrue(num_rows == num_cols)

  def test_gen_e_pos(self):
    magicNumber = 10
    for i in range(1, magicNumber):
      e = lab.gen_e(magicNumber)
      num_rows, num_cols = e.shape
      for j in range(num_rows):
        for k in range(num_cols):
          if j == k:
            self.assertTrue(e[j][k] == 1)
          else:
            self.assertTrue(e[j][k] == 0)

  def test_find_inverse(self):
    matrix = np.array([
      [1, 2],
      [3, 4]
    ])
    inverse_require = np.array([
      [-2, 1],
      [1.5, -0.5]
    ])
    lu, piv = lu_factor(matrix)
    inverse = lab.find_inverse(lu, piv)
    num_rows, num_cols = inverse.shape
    num_rows, num_cols = inverse_require.shape
    self.assertTrue(np.allclose(inverse, inverse_require))

  def test_norm(self):
    matrix = np.array([
      [1, 2],
      [3, 4]
    ])
    norm = 7
    self.assertEqual(lab.norm(matrix), norm)

  def test_residual(self):
    matrix = np.array([
      [1, 2],
      [3, 4]
    ])
    residual_require = np.array([
      [0, 0],
      [0, 0]
    ])
    residual = lab.get_residual_matrix(matrix)
    print(residual)
    self.assertTrue(np.allclose(residual, residual_require))

if __name__ == "__main__":
  unittest.main()