from scipy.linalg import lu_factor, lu_solve
import numpy as np 
from functools import reduce
import operator

def get_matrix(param, raw = False):
  if not(raw):
    a1 = param + 6
  else:
    a1 = "p + 6"
  return np.array([
    [a1, 2, 6, 8, -2, 1, 8, -5],
    [6, -22, -2, -1, 0, 5, -6, 4],
    [-2, -3, -16, 0, 0, -4, 2, -5],
    [1, 1, 4, 9, 1, 0, 0, -6],
    [0, 2, 0, 2, -3, -5, 7, 5],
    [6, -2, -4, 2, -8, -12, 3, -3],
    [-6, -6, 0, -8, 0, 5, -15, 0],
    [0, 7, 6, 0, -5, -8, -5, -3]
  ])

def gen_e(size):
  if size < 0:
    raise ValueError("Should be positive")
  A = []
  for i in range(size):
    A.append([])
    for j in range(size):
      if i == j:
        A[i].append(1)
      else:
        A[i].append(0)
  return np.array(A)

def find_inverse(lu, piv):
  num_rows, num_cols = lu.shape
  if num_rows != num_cols:
    raise RuntimeError("not square matrix")
  
  e = gen_e(num_rows)
  inverse = np.array([[0 for i in range(num_cols)] for j in range(num_cols)], dtype = float)
  for i in range(num_cols):
    columns_array = e[ :, i]
    vector = [[i] for i in columns_array]
    x = lu_solve((lu, piv), vector)
    for j in range(num_rows):
      inverse[j][i] = x[j][0]

  return inverse

def norm(matrix):
  max_row_sum = np.double(0)
  num_rows, num_cols = matrix.shape

  for i in range(num_rows):
    row_sum = reduce(operator.add, map(abs, matrix[i]))
    if row_sum > max_row_sum:
      max_row_sum = row_sum

  return max_row_sum 

def get_residual_matrix(matrix):
  num_rows, num_cols = matrix.shape
  lu, piv = lu_factor(matrix)
  inverse = find_inverse(lu, piv)
  r = np.subtract(gen_e(num_cols), np.matmul(matrix, inverse))
  return r

class Lab:
  p = [1.0, 0.1, 0.01, 0.0001, 0.000001, 0]

  def fun_a_cond(self, p):
    A = get_matrix(p)
    lu, piv = lu_factor(A)
    A_inverse = find_inverse(lu, piv)
    return norm(A) * norm(A_inverse)

  def fun_r_norm(self, p):
    A = get_matrix(p)
    residual = get_residual_matrix(A)
    return norm(residual)

  def __init__(self):
    self.matrices = []
    self.inverses = []
    self.r_norms = []
    self.a_cond_nums = []
    for i in Lab.p:
      A = get_matrix(i)      
      lu, piv = lu_factor(A)
      A_inverse = find_inverse(lu, piv)
      residual = get_residual_matrix(A)
      r_norm = norm(residual)
      self.a_cond_nums.append(norm(A) * norm(A_inverse))
      self.matrices.append(A)
      self.inverses.append(A_inverse)
      self.r_norms.append(r_norm)

if __name__ == "__main__":
  lab = Lab()
  for i in range(6):
    print(lab.p[i], end="   ")
    print(lab.a_cond_nums[i], end = "   ")
    print(lab.r_norms[i])
    