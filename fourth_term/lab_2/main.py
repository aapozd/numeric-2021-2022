from listings import *
import matplotlib
from utils import *

if __name__ == "__main__":
  add_table_format()
  lab = Lab()

  print_matrix(get_matrix(0, True), file = "fig/matrix_raw.tex")

  ax_vals = np.linspace(0, 1, 1000)
  # Создание файла.
  pdf = PdfPages("fig/fun_a_cond.pdf")
  fig, ax = plt.subplots()
  ax.plot(ax_vals, [lab.fun_a_cond(i) for i in ax_vals])
  ax.set_yscale('log')
  ax.set_xscale('log')
  #ax.set(xlim=(0, 0.01), xticks=np.arange(0, 0.01, 0.001), ylim=(0, 1e-06), yticks=np.arange(0, 1e-06, 1e-07))
  pdf.savefig()
  plt.close()
  pdf.close()

  ax_vals = np.linspace(0, 1, 10000)
  # Создание файла.
  pdf = PdfPages("fig/fun_r_norm.pdf")
  fig, ax = plt.subplots()
  ax.plot(ax_vals, [lab.fun_r_norm(i) for i in ax_vals])
  ax.set_yscale('log')
  ax.set_xscale('log')
  #ax.set(xlim=(0, 0.01), xticks=np.arange(0, 0.01, 0.001), ylim=(0, 1e-06), yticks=np.arange(0, 1e-06, 1e-07))
  pdf.savefig()
  plt.close()
  pdf.close()

  print_table("tables/p_norm_cond_table.tex", lab.p, lab.a_cond_nums, lab.r_norms)
