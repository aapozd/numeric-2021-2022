import tabulate as tbl
from tabulate import tabulate
from functools import partial
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def fprint(string, file = None):
  if file != None:
    with open(file, "w") as f:
      print(string, file = f)
  else:
    print(string)

def print_matrix(matrix, file = None):
  string = "\\[ \\begin{pmatrix}\n"
  num_rows, num_cols = matrix.shape
  for i in range(num_rows):
    for j in range(num_cols):
      string += str(matrix[i][j]) + " "
      if not((num_cols - j) == 1):
        string += "& "
    string += "\\\\\n"
  string += "\\end{pmatrix} \\]\n"
  
  fprint(string, file)

def border_line_begin_tabular(colwidths, colaligns, booktabs=False, longtable=False):
  alignment = {"left": "|l", "right": "|r", "center": "|c", "decimal": "|r"}
  tabular_columns_fmt = "".join([alignment.get(a, "l") for a in colaligns]) + "|"
  return "\n".join(
    [
      ("\\begin{tabular}{" if not longtable else "\\begin{longtable}{")
      + tabular_columns_fmt
      + "}",
      "\\toprule" if booktabs else "\\hline",
    ]
  )
  
def add_table_format():
  form = tbl._table_formats["latex_raw"]
  tbl._table_formats["latex_border_longtable"] = tbl.TableFormat(
    lineabove=partial(border_line_begin_tabular, longtable=True),
    linebelowheader=tbl.Line("\\hline\n\\endhead", "", "", ""),
    linebetweenrows=None,
    linebelow=tbl.Line("\\hline\n\\end{longtable}", "", "", ""),
    headerrow=partial(tbl._latex_row, escrules={}),
    datarow=partial(tbl._latex_row, escrules={}),
    padding=1,
    with_header_hide=None,
  )

def print_table(filename, headers, columns_or_rows, columns_mode = True, precision = 2):
  if columns_mode:
    rows = []
    for i in range(len(columns[0])):
      row = []
      for column in columns:
        row.append(column[i])
      rows.append(row)
  else:
    rows = columns_or_rows
    
  with open(filename, 'w') as f:
    floatfmt = ".{prec}f".format(prec = precision)
    table = tabulate(rows, headers, tablefmt="latex_border_longtable", floatfmt = floatfmt)
    print(table, file = f)