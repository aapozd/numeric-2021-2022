import numpy as np
from scipy.integrate import RK45, odeint

def fun(t, x):
  return np.array([
    -430 * x[0] - 12000 * x[1] + np.exp(-10 * t),
    x[0] + np.log(1 + 100 * np.power(t, 2))
  ])

def runge_step(t, x, h):
  # both are vectors
  x_n_1_3 = x + h / 3 * fun(t, x)
  x_n_p_1 = x + h / 2 * (-fun(t, x) + 3 * fun(t + h/3, x_n_1_3))
  return x_n_p_1

def runge_solve(t0, x0, h, t_out, h_int):
  solutions = []
  t = t0
  x = x0
  for t in np.arange(t0, t_out, h):
    for t_int in np.arange(t, t + h, h_int):
      x = runge_step(t_int, x, h_int)
    
    solutions.append((t + h, x[0], x[1]))

  return np.array(solutions)

def solve_rkf_2(h_print, t_0, t_1, y_0, eps):
  solutions = []
  t_bound = h_print
  for j in np.arange(h_print, t_1 + h_print, h_print):
    rkf45 = RK45(fun, t0 = t_0, y0 = y_0, t_bound = t_bound, atol = eps, rtol = eps)
    t_bound += h_print
    while rkf45.status != "finished":
      rkf45.step()
    solutions.append((rkf45.t, rkf45.y[0], rkf45.y[1]))

  return np.array(solutions)

class Lab:
  h_print = 0.0075
  h_int = 0.0075
  h_int_stable = 0.00001
  eps = 0.000001
  x_1 = 3
  x_2 = -1
  t_0 = 0
  t_1 = 0.15

  def __init__(self):
    self.rkf45_solutions = solve_rkf_2(Lab.h_print, Lab.t_0, Lab.t_1, [Lab.x_1, Lab.x_2], Lab.eps)
    self.runge_2_solutions_h_int = runge_solve(Lab.t_0, [Lab.x_1, Lab.x_2], Lab.h_print, Lab.t_1, Lab.h_int)
    self.runge_2_solutions_stable = runge_solve(Lab.t_0, [Lab.x_1, Lab.x_2], Lab.h_print, Lab.t_1, Lab.h_int_stable)
  
if __name__ == "__main__":
  lab = Lab()

  print(lab.rkf45_solutions)
  print(lab.runge_2_solutions_h_int)
  print(lab.h_int_stable)
  print(lab.runge_2_solutions_stable)