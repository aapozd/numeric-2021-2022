from listings import *
from utils import *
import numpy as np

if __name__ == "__main__":
  lab = Lab()

  precision = 6
  headers = ["t", "\( x_1(t) \)", "\( x_2(t) \)"]
  print_table("tables/rkf45_table.tex", headers, lab.rkf45_solutions, columns_mode = False, precision = precision)
  print_table("tables/runge_2_h_int_table.tex", headers, lab.runge_2_solutions_h_int, columns_mode = False, precision = precision)
  fprint("\( h_{stable} = " + str(lab.h_int_stable) + " \)", "report/h_int_stable.tex")
  print_table("tables/runge_2_stable_table.tex", headers, lab.runge_2_solutions_stable, columns_mode = False, precision = precision)